alias rm="rm -v"
alias mv="mv -v"
alias cp="cp -v"

alias skype="LD_LIBRARY_PATH=/usr/lib/i386-linux-gnu/ skype"

alias rabbit_list="rabbitmqadmin list queues name consumers messages messages_unacknowledged messages_ready"

alias gti="git"
alias gut="git"
alias igt="git"

alias taks="task"
alias vi="kak"
